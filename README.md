# Tokyo Admin Panel

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```

## Solucion a nuevos errores en el proyecto

```
Import trace for requested module: __barrel_optimize__?names=alpha,createTheme,darken!=!./node_modules/@mui/material/index.js ./src/theme/schemes/NebulaFighterTheme.ts ./src/theme/base.ts ./src/theme/ThemeProvider.tsx
```

- la razon de esa advertencia que se prsentava en la terminal es la siguiente

```ts
import { alpha, createTheme, darken } from "@mui/material";
```

- razones que asta este momento desconosco
- solucion

```ts
import { alpha, createTheme, darken } from "@mui/material/styles";
```
