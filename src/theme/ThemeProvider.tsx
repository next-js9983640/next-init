"use client"
import { ReactNode, createContext, useState, useEffect } from 'react'
import ThemeProvider from "@mui/material/styles/ThemeProvider"
import { themeCreator } from './base'


export const ThemeContext = createContext((_themeName: string): void => { });
export function ThemeProviderWrapper({ children }: { children: ReactNode }) {
    const [themeName, setThemeName] = useState('NebulaFighterTheme')

    useEffect(() => {
        const currentTheme = window.localStorage.getItem('appTheme') || "NebulaFighterTheme"
        setThemeName(currentTheme)
    }, [])

    const theme = themeCreator(themeName)
    const saveTheme = (themeName: string) => {
        window.localStorage.setItem('appTheme', themeName)
    }
    return (
        <ThemeContext.Provider value={saveTheme}>
            <ThemeProvider theme={theme}>
                {children}
            </ThemeProvider>
        </ThemeContext.Provider>

    )
}
