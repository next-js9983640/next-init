import React from 'react'

function Logo() {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            width={100}
            height={100}
            viewBox="0 0 64 64"
        >
            <linearGradient
                id="a"
                x1={32}
                x2={32}
                y1={22.915}
                y2={39.482}
                gradientUnits="userSpaceOnUse"
            >
                <stop offset={0} stopColor="#6dc7ff" />
                <stop offset={1} stopColor="#e6abff" />
            </linearGradient>
            <path fill="url(#a)" d="m32 24-16 7 16 9 16-9z" />
            <linearGradient
                id="b"
                x1={32}
                x2={32}
                y1={4.319}
                y2={56.536}
                gradientUnits="userSpaceOnUse"
            >
                <stop offset={0} stopColor="#1a6dff" />
                <stop offset={1} stopColor="#c822ff" />
            </linearGradient>
            <path
                fill="url(#b)"
                d="m15.979 32.214 15.029 8.588a2.003 2.003 0 0 0 1.983-.001l15.028-8.588a2.01 2.01 0 0 0 .949-1.257 2.006 2.006 0 0 0-.254-1.553L33.687 5.789A1.99 1.99 0 0 0 32 4.863c-.688 0-1.318.346-1.688.926L15.284 29.404a2.006 2.006 0 0 0-.254 1.553c.13.529.477.987.949 1.257zm1.619-1.376L31 25.477v13.017l-13.402-7.656zM33 25.477l13.4 5.36L33 38.495V25.477zm12.71 2.93L33 23.323V8.434l12.71 19.973zM31 8.434v14.889l-12.71 5.084L31 8.434z"
            />
            <linearGradient
                id="c"
                x1={32}
                x2={32}
                y1={4.319}
                y2={56.536}
                gradientUnits="userSpaceOnUse"
            >
                <stop offset={0} stopColor="#1a6dff" />
                <stop offset={1} stopColor="#c822ff" />
            </linearGradient>
            <path
                fill="url(#c)"
                d="M50.178 35.027a1.001 1.001 0 0 0-1.255-.19L32 44.923 15.077 34.837a1 1 0 0 0-1.313 1.458L30.39 58.5c.381.518.968.814 1.61.814s1.229-.296 1.605-.808l16.63-22.211a1 1 0 0 0-.057-1.268zm-31.782 4.116L31 46.655v9.323L18.396 39.143zM33 55.968v-9.313l12.602-7.511L33 55.968z"
            />
        </svg>
    )
}

export default Logo