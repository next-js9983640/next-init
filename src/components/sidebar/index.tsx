"use client"
import Logo from '@/icons/logo'
import NextLink from 'next/link';
import { useState, useContext } from 'react'
import Link from 'next/link'

import Box, { BoxProps } from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import Button from '@mui/material/Button';

import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import InboxIcon from '@mui/icons-material/Inbox';
import DraftsIcon from '@mui/icons-material/Drafts';


// icons


import BrightnessLowTwoToneIcon from '@mui/icons-material/BrightnessLowTwoTone';
import AccountTreeIcon from '@mui/icons-material/AccountTree';

import { SidebarContext } from '@/context/SidebarContext'

import Scrollbar from '@/components/Scrollbar'

import {
  ListSubheader,
  alpha,
  useTheme,
  styled
} from '@mui/material';


import { data, sidebard } from '@/data/SidebarData'

function Item(props: BoxProps) {
  const { sx, ...other } = props;
  return (
    <Box
      sx={{
        color: (theme) => (theme.palette.mode === 'dark' ? 'grey.300' : 'grey.800'),
        fontSize: '0.875rem',
        fontWeight: '700',
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        ...sx,
      }}
      {...other}
    />
  );
}


const SubMenuWrapper = styled(Box)(
  ({ theme }) => `
    .MuiList-root {

      .MuiListItem-root {
        padding: 1px 0;

        .MuiBadge-root {
          position: absolute;
          right: ${theme.spacing(3.2)};

          .MuiBadge-standard {
            background: ${theme.colors.primary.main};
            font-size: ${theme.typography.pxToRem(10)};
            font-weight: bold;
            text-transform: uppercase;
            color: ${theme.palette.primary.contrastText};
          }
        }
    
        .MuiButton-root {
          display: flex;
          color: ${theme.colors.alpha.trueWhite[70]};
          background-color: transparent;
          width: 100%;
          justify-content: flex-start;
          padding: ${theme.spacing(1.2, 3)};

          .MuiButton-startIcon,
          .MuiButton-endIcon {
            transition: ${theme.transitions.create(['color'])};

            .MuiSvgIcon-root {
              font-size: inherit;
              transition: none;
            }
          }

          .MuiButton-startIcon {
            color: ${theme.colors.alpha.trueWhite[30]};
            font-size: ${theme.typography.pxToRem(20)};
            margin-right: ${theme.spacing(1)};
          }
          
          .MuiButton-endIcon {
            color: ${theme.colors.alpha.trueWhite[50]};
            margin-left: auto;
            opacity: .8;
            font-size: ${theme.typography.pxToRem(20)};
          }

          &.active,
          &:hover {
            background-color: ${alpha(theme.colors.alpha.trueWhite[100], 0.06)};
            color: ${theme.colors.alpha.trueWhite[100]};

            .MuiButton-startIcon,
            .MuiButton-endIcon {
              color: ${theme.colors.alpha.trueWhite[100]};
            }
          }
        }

        &.Mui-children {
          flex-direction: column;

          .MuiBadge-root {
            position: absolute;
            right: ${theme.spacing(7)};
          }
        }

        .MuiCollapse-root {
          width: 100%;

          .MuiList-root {
            padding: ${theme.spacing(1, 0)};
          }

          .MuiListItem-root {
            padding: 1px 0;

            .MuiButton-root {
              padding: ${theme.spacing(0.8, 3)};

              .MuiBadge-root {
                right: ${theme.spacing(3.2)};
              }

              &:before {
                content: ' ';
                background: ${theme.colors.alpha.trueWhite[100]};
                opacity: 0;
                transition: ${theme.transitions.create([
    'transform',
    'opacity'
  ])};
                width: 6px;
                height: 6px;
                transform: scale(0);
                transform-origin: center;
                border-radius: 20px;
                margin-right: ${theme.spacing(1.8)};
              }

              &.active,
              &:hover {

                &:before {
                  transform: scale(1);
                  opacity: 1;
                }
              }
            }
          }
        }
      }
    }
`
);

function NavBar() {
  const { closeSidebar } = useContext(SidebarContext);
  const theme = useTheme();
  const [open, setOpen] = useState(true);
  return (
    <div style={{ height: '94vh', display: 'flex', width: "100%", justifyContent: 'center', flexDirection: 'column' }}>
      <Box sx={{ flex: 1, display: 'flex', flexDirection: 'column', gap: '10px' }}>
        <Item><Logo /></Item>
        <Divider />
        <Box sx={{ textAlign: 'center' }}>Convenciones de enrutamiento</Box>
        <Divider />
        <Box sx={{ flex: 1 }}>
          <Scrollbar>
            {data.map((item: sidebard) => (
              <>
                <Divider
                  sx={{
                    mt: theme.spacing(3),
                    mx: theme.spacing(2),
                    background: theme.colors.alpha.trueWhite[10]
                  }}
                />
                <List
                  key={item.title}
                  component="div"
                  subheader={
                    <ListSubheader component="div" disableSticky>
                      {item.title}
                    </ListSubheader>
                  }
                >
                  <Divider />
                  <SubMenuWrapper>
                    <List component="div">
                      {item.children?.map((child: any) => (
                        <ListItem component="div" key={child.title}>
                          <Button
                            disableRipple
                            onClick={closeSidebar}
                            startIcon={<AccountTreeIcon />}
                          >
                            {child.title}
                          </Button>
                        </ListItem>
                      ))}

                    </List>
                  </SubMenuWrapper>
                </List>

              </>
            ))}
          </Scrollbar>
        </Box>

        <Divider />
        <Item sx={{ width: '100%' }}><Button sx={{ width: '100%' }} variant="contained" color='error'>Logout</Button></Item>
      </Box>
    </div>
  )
}

export default NavBar


