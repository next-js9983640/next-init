export type sidebard = {
  title: string;
  children?: object[];
};

export const data: sidebard[] = [
  {
    title: "Routing Files",
    children: [
      {
        title: "layout",
      },
      {
        title: "page",
      },
      {
        title: "loading",
      },
      {
        title: "not-found",
      },
      {
        title: "error",
      },
      {
        title: "global-error",
      },
      {
        title: "route",
      },
      {
        title: "template",
      },
      {
        title: "default",
      },
    ],
  },
  {
    title: "Nested Routes",
    children: [
      {
        title: "folder",
      },
      {
        title: "folder/folder",
      }
    ],
  },
  {
    title: "Dinamic Routes",
    children: [
      {
        title: "[folder]",
      },
      {
        title: "[...folder]",
      },
      {
        title: "[[...folder]]",
      }
    ],
  },
  {
    title: "Route Groups an private Folders",
    children: [
      {
        title: "(folder)",
      },
      {
        title: "_folder",
      }
    ],
  },
  {
    title: "Paraller and Intercepted Routes",
    children: [
      {
        title: "@folder",
      },
      {
        title: "(.)folder",
      },
      {
        title: "(..)folder",
      },
      {
        title: "(..)(..)folder",
      },
      {
        title: "(...)folder",
      },
    ],
  },
  {
    title: "Metadata File Convention",
    children: [
      {
        title: "favicon",
      },
      {
        title: "icon",
      },
      {
        title: "apple-icon",
      }
    ],
  },
];
