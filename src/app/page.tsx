import Image from 'next/image'
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import Main from '@/layouts/main'

export default function Home() {
  return (
    <Main>
      <Stack spacing={2} direction="row">
        <Button variant="contained" color='secondary'>Text</Button>
        <Button variant="contained" color='primary'>Contained</Button>
        <Button variant="contained" color='success'>Outlined</Button>
        <Button variant="contained" color='error'>Logout</Button>
      </Stack>
    </Main>
  )
}
