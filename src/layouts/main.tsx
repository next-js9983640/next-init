import Box from '@mui/material/Box';
import Sidebar from '@/components/sidebar'

function Main({ children }: { children: React.ReactNode }) {
    return (
        <Box
            sx={{
                width: '100%',
                height: 'auto',
                color: '#fff',
                '& > .MuiBox-root > .MuiBox-root': {
                    p: 1,
                    borderRadius: 2,
                    fontSize: '0.875rem',
                    fontWeight: '700',
                },
            }}
        >
            <Box
                sx={{
                    display: 'grid',
                    gridTemplateColumns: 'repeat(6, 1fr)',
                    
                    gap: 1,
                    gridTemplateRows: 'repeat(14, 1fr)',
                    gridTemplateAreas: `"sidebar header header header header header"
                                        "sidebar main main main main main"
                                        "sidebar main main main main main"
                                        "sidebar main main main main main"
                                        "sidebar main main main main main"
                                        "sidebar main main main main main"
                                        "sidebar main main main main main"
                                        "sidebar main main main main main"
                                        "sidebar main main main main main"
                                        "sidebar main main main main main"
                                        "sidebar main main main main main"
                                        "sidebar main main main main main"
                                        "sidebar main main main main main"
                                        "sidebar footer footer footer footer footer"`,
                }}
            >
                <Box sx={{ gridArea: 'header',  border: '2px solid #8C7CF0' }}>Header</Box>
                <Box sx={{ gridArea: 'sidebar', border: '2px solid #8C7CF0' }}><Sidebar /></Box>
                <Box sx={{ gridArea: 'main',  border: '2px solid #8C7CF0' }}>{children}</Box>
                <Box sx={{ gridArea: 'footer',  border: '2px solid #8C7CF0' }}>Footer</Box>
            </Box>
        </Box>
    )
}

export default Main